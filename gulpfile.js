var gulp = require('gulp');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var mincss = require('gulp-clean-css');

var dircss = [
    'src/css/*.css'
];

gulp.task('taskCss', function () {
    gulp.src(dircss)
        .pipe(plumber())
        .pipe(concat('style.min.css'))
        .pipe(mincss())
        .pipe(gulp.dest('src/'));
});

gulp.task('default', function () {
    gulp.watch(dircss, ['taskCss']);
});